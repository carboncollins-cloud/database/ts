terraform {
  required_providers {
    consul = {
      source = "hashicorp/consul"
      version = "2.17.0"
    }
  }
}

resource "consul_config_entry" "influxdb_intention" {
  name = "influxdb"
  kind = "service-intentions"

  config_json = jsonencode({
    Sources = [
      {
        Action     = "allow"
        Name       = "grafana"
        Precedence = 9
        Type       = "consul"
      },
      {
        Action     = "allow"
        Name       = "internal-proxy"
        Precedence = 9
        Type       = "consul"
      }
    ]
  })
}
