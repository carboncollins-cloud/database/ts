terraform {
  required_providers {
    vault = {
      source = "hashicorp/vault"
      version = "3.15.2"
    }

    random = {
      source = "hashicorp/random"
      version = "3.5.1"
    }
  }
}

locals {
  db_root_user = "c3-init"
}

resource "random_password" "db_initial_password" {
  length           = 24

  lower            = true
  upper            = true
  numeric          = true
  special          = true

  override_special = "!#$%&*()-_=+[]{}<>:?"

  keepers = {
    datacenter = "soc"
    region     = "se"
    version    = "2"
  }
}

# resource "vault_mount" "db" {
#   path = "c3tsdb"
#   type = "database"
# }

# resource "vault_database_secret_backend_connection" "influxdb" {
#   backend       = vault_mount.db.path
#   name          = "influxdb"
#   plugin_name   = "influxdb-database-plugin"
#   allowed_roles = []

#   influxdb {
#     host = "influxdb.soc.carboncollins.se"
#     port = "443"

#     tls = true

#     username = local.db_root_user
#     password = random_password.db_initial_password.result

#     username_template = "{{ printf 'dyn_%s_%s_%s' (.DisplayName | truncate 10) (.RoleName) (random 4) }}"
#   }
# }

resource "vault_kv_secret_v2" "example" {
  mount  = "c3kv"
  name   = "datacenter/soc/database/influxdb"

  data_json                  = jsonencode({
    initialUsername     = local.db_root_user,
    initialPassword     = random_password.db_initial_password.result
    initialOrganisation = "carboncollins-cloud"
    initialBucket       = "c3"
  })

  custom_metadata {
    max_versions = 2
  }
}
