terraform {
  backend "consul" {
    path = "terraform/database-timeseries"
  }

  required_providers {
    nomad = {
      source = "hashicorp/nomad"
      version = "1.4.19"
    }

    consul = {
      source = "hashicorp/consul"
      version = "2.17.0"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.15.2"
    }

    random = {
      source = "hashicorp/random"
      version = "3.5.1"
    }
  }
}

provider "nomad" {}
provider "consul" {}
provider "vault" {}
provider "random" {}

module "intentions" {
  source = "../../modules/influxDbConsulIntentions"
}

module "vault_integration" {
  source = "../../modules/influxDbVaultIntegration"
}
