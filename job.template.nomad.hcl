job "database-ts" {
  name        = "Database Time Series (InfluxDB)"
  type        = "service"
  region      = "se"
  datacenters = ["soc"]
  namespace   = "c3-database"

  priority = 60

  vault {
    policies = ["service-influxdb"]
  }

  group "influxdb" {
    count = 1

    network {
      mode = "bridge"
    }

    volume "influx-data" {
      type      = "host"
      read_only = false
      source    = "influx-data"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    service {
      provider = "consul"
      name     = "influxdb"
      port     = "8086"
      task     = "influxdb"

      connect {
        sidecar_service {}
      }

      check {
        expose   = true
        name     = "Application Health Status"
        type     = "http"
        path     = "/health"
        interval = "10s"
        timeout  = "3s"
      }

      tags = [
        "internal-proxy.enable=true",
        "internal-proxy.consulcatalog.connect=true",
        "internal-proxy.http.routers.influxdb.entrypoints=https",
        "internal-proxy.http.routers.influxdb.rule=Host(`influxdb.soc.carboncollins.se`)",
        "internal-proxy.http.routers.influxdb.tls=true",
        "internal-proxy.http.routers.influxdb.tls.certresolver=lets-encrypt",
        "internal-proxy.http.routers.influxdb.tls.domains[0].main=*.soc.carboncollins.se"
      ]
    }

    task "influxdb" {  
      driver = "docker"
      leader = true

      volume_mount {
        volume      = "influx-data"
        destination = "/var/lib/influxdb2"
        read_only   = false
      }

      resources {
        cpu    = 2000
        memory = 3072
      }

      config {
        image = "influxdb:2.7.1-alpine"

        // volumes = [
        //   "local/config/:/etc/influxdb2/influx-configs"
        // ]
      }

      env {
        TZ = "Europe/Stockholm"
        INFLUX_CONFIGS_PATH = "/var/lib/influxdb2/influx-config"
      }

      template {
        data = <<EOH
        {{ with secret "c3kv/data/datacenter/soc/database/influxdb" }}
        DOCKER_INFLUXDB_INIT_MODE = "setup"
        DOCKER_INFLUXDB_INIT_ORG = {{ index .Data.data "initialOrganisation" }}
        DOCKER_INFLUXDB_INIT_BUCKET = {{ index .Data.data "initialBucket" }}
        DOCKER_INFLUXDB_INIT_USERNAME = {{ index .Data.data "initialUsername" }}
        DOCKER_INFLUXDB_INIT_PASSWORD = {{ index .Data.data "initialPassword" }}
        {{ end }}
        EOH

        destination = "secrets/influxdb2.env"
        env = true
      }
    }
  }

  reschedule {
    delay          = "10s"
    delay_function = "exponential"
    max_delay      = "10m"
    unlimited      = true
  }

  update {
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "10m"
    progress_deadline = "15m"
    auto_revert       = true
  }

  meta {
    gitSha      = "[[ .gitSha ]]"
    gitBranch   = "[[ .gitBranch ]]"
    pipelineId  = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId   = "[[ .projectId ]]"
    projectUrl  = "[[ .projectUrl ]]"
    statefull   = "true"
  }
}
